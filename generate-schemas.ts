#!/usr/bin/env node
import * as path from 'path';
import * as process from 'process';

import { hideBin } from 'yargs/helpers';
import * as yargs from 'yargs';
import 'source-map-support/register';

import { generateSchemasSync } from './src';
import { GenerateSchemasOptions, getEnvOptions, getPackageJsonOptions } from './src/options';
import { DEFAULT_SCHEMA_OUTPUT_DIR } from './src/schemas';


const args = yargs(hideBin(process.argv))
    .usage('Usage: generate-schemas <package-path> [options]')
    .options({
        project: {
            type: 'string',
            alias: 'p',
            describe: 'Path to tsconfig.json; defaults to <package-path>/tsconfig.json',
        },
        output: {
            type: 'string',
            alias: 'o',
            describe: `Directory where output schemas will be stored; defaults to <package-path>/${DEFAULT_SCHEMA_OUTPUT_DIR}/`,
        },
        ignore: {
            type: 'string',
            array: true,
            alias: 'i',
            describe: 'A list of glob patterns. Any file matching one of these will be ignored during schema generation',
        },
        filter: {
            type: 'string',
            array: true,
            alias: 'f',
            describe: 'A list of glob patterns. Only files matching one of these will be parsed during schema generation',
        },
        debug: {
            type: 'boolean',
            alias: 'd',
            describe: 'Increase logging verbosity',
        },
    })
    .check((argv) => {
        if (argv._.length !== 1) throw new Error(`<package-path> is required`);
        return true;
    })
    .help()
    .parseSync();

const packagePath = args._[0] as string;
const packageJsonOptions = getPackageJsonOptions(packagePath);
const envOptions = getEnvOptions();
const options: GenerateSchemasOptions = {
    packagePath,
    tsconfigPath: args.project
        || envOptions.tsconfigPath
        || packageJsonOptions.tsconfigPath
        || path.resolve(packagePath, 'tsconfig.json'),
    schemaOutputDirectory: args.output
        || envOptions.schemaOutputDirectory
        || packageJsonOptions.schemaOutputDirectory
        || path.resolve(packagePath, DEFAULT_SCHEMA_OUTPUT_DIR),
    filter: args.filter
        || envOptions.filter
        || packageJsonOptions.filter
        || undefined,
    ignore: args.ignore
        || envOptions.ignore
        || packageJsonOptions.ignore
        || undefined,
    debug: [
        args.debug,
        envOptions.debug,
        packageJsonOptions.debug,
    ].find(v => v !== undefined),
};

const tStart = Date.now();
options.debug && console.log(`Generating JSON-schemas from package ${options.packagePath}`);
const generatedSchemaCount = generateSchemasSync(options);
options.debug && console.log(`${generatedSchemaCount} JSON-schemas generated in ${Date.now() - tStart}ms`);
