# Configuration

[[_TOC_]]

## Configurable properties

### Package path (`<package-path>`)

Path to the directory containing the package.json file.

This configuration is only available during the schema generation step, during which it must be explicitly set. In most cases, it should simply be set to `.` when calling the `generate-schemas` command (`generate-schemas .`).

This value is automatically computed at runtime by detecting the closest `package.json` to `createTypeValidator`/`createTypeGuard` call site.

### tsconfig.json location

Path (relative to `<package-path>`) to a tsconfig.json file.

This configuration is only available during the schema generation step. It defaults to `<package-path>/tsconfig.json`.

Can be overridden (in this order of priority):
- with the `--project`/`-p` option when calling the `generate-schemas` command
- by setting `TSCONFIG_PATH` environment variable
- by setting `auto-type-guard.tsconfigPath` in your `package.json`

### Schema output directory

Path (relative to `<package-path>`) to the directory where JSON-schemas will be written to during schema generation, and read from at runtime.

If this configuration is set to a non-default value during the schema generation step, it *must* also be set at runtime (through env variable or package.json).

Defaults to `<package-path>/json-schemas`.

Can be overridden:
- with the `--output`/`-o` option when calling the `generate-schemas` command
- by setting `SCHEMA_OUTPUT_DIRECTORY` environment variable
- by setting `auto-type-guard.schemaOutputDirectory` in your `package.json`

### Ignore list

A list of glob patterns, the files matching any of these will be ignored during the schema generation.

Defaults to an empty list.

Can be overridden:
- with the `--ignore`/`-i` option when calling the `generate-schemas` command
- by setting `IGNORE` environment variable (separate patterns with a `;` character)
- by setting `auto-type-guard.ignore` in your `package.json`

### Filter list

A list of glob patterns, only the files matching one of these will be parsed during the schema generation.

Defaults to `undefined`.

Can be overridden:
- with the `--filter`/`-f` option when calling the `generate-schemas` command
- by setting `FILTER` environment variable (separate patterns with a `;` character)
- by setting `auto-type-guard.filter` in your `package.json`

If both `ignore` and `filter` options are set, only files matching one of the `filter` pattern _and_ matching none of the `ignore` patterns will be parsed.

## Examples

### Basic example

[~/my-package/package.json]
```json
{
  "name": "my-package",
  "dependencies": {
    "auto-type-guard": "^0.0.1"
  },
  "devDependencies": {
    "typescript": "^4.3.5"
  },
  "scripts": {
    "build": "tsc -p tsconfig.json",
    "postbuild": "generate-schemas ."
  }
}
```

[~/my-package/main.ts]

```ts
import { createTypeGuardSync } from 'auto-type-guard';

type MyType = {
  name: string;
};

const myTypeGuard = createTypeGuardSync<MyType>('MyType');

const unknownData: unknown = {
  name: 'John Doe',  
};
if (myTypeGuard(unknownData)) {
    const name = unknownData.name;
}
```

### Environment variable configuration

[~/my-package/.env]
```
SCHEMA_OUTPUT_DIRECTORY=static/schemas
TSCONFIG_PATH=tsconfig.prod.json
DEBUG=1
```

[~/my-package/package.json]
```json
{
  "name": "my-package",
  "dependencies": {
    "auto-type-guard": "^0.0.1",
    "dotenv": "^10.0.0"
  },
  "devDependencies": {
    "typescript": "^4.3.5"
  },
  "scripts": {
    "build": "tsc -p tsconfig.prod.json",
    "postbuild": "node -r dotenv/config node_modules/.bin/generate-schemas ."
  }
}
```

[~/my-package/main.ts]

```ts
require('dotenv').config();

import { createTypeGuardSync } from 'auto-type-guard';

type MyType = {
  name: string;
};

const myTypeGuard = createTypeGuardSync<MyType>('MyType');

const unknownData: unknown = {
  name: 'John Doe',  
};
if (myTypeGuard(unknownData)) {
    const name = unknownData.name;
}
```

### package.json configuration

[~/my-package/package.json]

```json
{
  "name": "my-package",
  "dependencies": {
    "auto-type-guard": "^0.0.1"
  },
  "devDependencies": {
    "typescript": "^4.3.5",
    "rimraf": "3.0.2"
  },
  "scripts": {
    "build": "tsc -p tsconfig.prod.json",
    "postbuild": "generate-schemas .",
    "pretest": "tsc -p tsconfig.test.json && generate-schemas . -p tsconfig.test.json -f '**/test/**' -o static/test-json-schemas",
    "test": "mocha test/**/*.spec.ts",
    "posttest": "rimraf static/test-json-schemas"
  },
  "auto-type-guard": {
    "tsconfigPath": "tsconfig.prod.json",
    "schemaOutputDirectory": "static/schemas",
    "filter": [
      "**/src/**"
    ]
  }
}
```

Note that in the `pretest` script, the command line options take precedence over the package.json configuration.

[~/my-package/main.ts]

```ts
import { createTypeGuardSync } from 'auto-type-guard';

type MyType = {
  name: string;
};

const myTypeGuard = createTypeGuardSync<MyType>('MyType');

const unknownData: unknown = {
  name: 'John Doe',  
};
if (myTypeGuard(unknownData)) {
    const name = unknownData.name;
}
```
