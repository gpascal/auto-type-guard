# TODO

- [x] filter paths from which to detect calls to `createTypeValidator`/`createTypeGuard`
- [ ] more tests
- [x] linter
- [ ] cleaner logging
- [ ] improve errors:
  - [x] create a custom error class to ease error processing at runtime
  - [x] add details as to why the validation failed
  - [x] throw custom errors when AST parsing fails
  - [ ] throw custom errors for known cases: schema file not found at runtime, generating a schema for an overloaded interface, non-aliased type argument...
