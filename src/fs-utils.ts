import * as fs from 'fs';
import * as path from 'path';
import CallSite = NodeJS.CallSite;

import { TypeGuardError } from './error';


export function getCallerFilePath(depth = 0): string {
    // noinspection JSUnusedLocalSymbols
    const [
        _newErrorCall,
        _getRawStackCall,
        _getCallerFilepathCall,
        ...rawStack
    ] = getRawStack();

    const callSite = rawStack[depth];
    if (callSite === undefined) {
        throw new TypeGuardError(`Could get caller path at depth ${depth}`);
    }
    return callSite.getFileName() as string;
}

// https://v8.dev/docs/stack-trace-api
function getRawStack(): CallSite[] {
    const origPrepareStackTrace = Error.prepareStackTrace;
    Error.prepareStackTrace = (error: Error, stack: CallSite[]) => stack;
    const rawStack = new Error().stack as unknown as CallSite[];
    Error.prepareStackTrace = origPrepareStackTrace;

    return rawStack;
}

export function getPackagePathSync(startingPath: string): string {
    let p = startingPath;
    while (p !== '/') {
        const candidate = path.resolve(p, 'package.json');
        if (fs.existsSync(candidate)) {
            return p;
        }
        p = path.resolve(p, '..');
    }
    throw new TypeGuardError(`Could not find package.json starting from path ${startingPath}`);
}

export async function getPackagePath(startingPath: string): Promise<string> {
    let p = startingPath;
    while (p !== '/') {
        const candidate = path.resolve(p, 'package.json');
        try {
            await fs.promises.access(candidate);
            return p;
        } catch {
            p = path.resolve(p, '..');
        }
    }
    throw new TypeGuardError(`Could not find package.json starting from path ${startingPath}`);
}
