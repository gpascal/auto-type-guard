import { default as Ajv, ValidateFunction } from 'ajv';

import { loadSchema, loadSchemaSync } from '.';
import { TypeGuardError } from './error';


const ajv = new Ajv();

export type TypeValidator<T> = (object: unknown) => asserts object is T;
export type TypeGuard<T> = (object: unknown) => object is T;

export async function createTypeValidator<T>(typeName: string): Promise<TypeValidator<T>> {
    const validationFunction = await getValidationFunction<T>(typeName);
    return typeValidatorFactory<T>(typeName, validationFunction);
}

export function createTypeValidatorSync<T>(typeName: string): TypeValidator<T> {
    const validationFunction = getValidationFunctionSync<T>(typeName);
    return typeValidatorFactory<T>(typeName, validationFunction);
}

function typeValidatorFactory<T>(typeName: string, validationFunction: ValidateFunction): TypeValidator<T> {
    return (object: unknown): asserts object is T => {
        if (!validationFunction(object)) {
            throw new TypeGuardError(`Input is not a valid instance of type ${typeName}`);
        }
    };
}

// Create type checker

export async function createTypeGuard<T>(typeName: string): Promise<TypeGuard<T>> {
    const validationFunction = await getValidationFunction<T>(typeName);
    return typeGuardFactory<T>(typeName, validationFunction);
}

export function createTypeGuardSync<T>(typeName: string): TypeGuard<T> {
    const validationFunction = getValidationFunctionSync<T>(typeName);
    return typeGuardFactory<T>(typeName, validationFunction);
}

function typeGuardFactory<T>(typeName: string, validationFunction: ValidateFunction): TypeGuard<T> {
    return (object: unknown): object is T => validationFunction(object);
}

// Generate validation function

const validationFunctionCache = new Map<string, ValidateFunction>();

async function getValidationFunction<T>(typeName: string): Promise<ValidateFunction> {
    if (!validationFunctionCache.has(typeName)) {
        const schema = await loadSchema(typeName);
        validationFunctionCache.set(typeName, ajv.compile<T>(schema));
    }
    return validationFunctionCache.get(typeName)!;
}

function getValidationFunctionSync<T>(typeName: string): ValidateFunction {
    if (!validationFunctionCache.has(typeName)) {
        const schema = loadSchemaSync(typeName);
        validationFunctionCache.set(typeName, ajv.compile<T>(schema));
    }
    return validationFunctionCache.get(typeName)!;
}
