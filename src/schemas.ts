import * as path from 'path';
import * as fs from 'fs';

import { CallExpression, Node, Project, SyntaxKind, TypeChecker } from 'ts-morph';
import * as tsj from 'ts-json-schema-generator';
import * as minimatch from 'minimatch';
import type { JSONSchema7 as JsonSchema } from 'json-schema';

import { createTypeGuard, createTypeGuardSync, createTypeValidator, createTypeValidatorSync } from '.';
import { getCallerFilePath, getPackagePath, getPackagePathSync } from './fs-utils';
import { GenerateSchemasOptions, getEnvOptions, getPackageJsonOptions } from './options';
import { TypeGuardError, wrapError, wrapErrorAsync } from './error';


const VALIDATOR_TS_FILENAME = 'validator.ts';
const VALIDATOR_DTS_FILENAME = 'validator.d.ts';
export const DEFAULT_SCHEMA_OUTPUT_DIR = 'json-schemas';

export async function generateSchemas(options: GenerateSchemasOptions): Promise<number> {
    const project = createProject(options);
    const references = findReferences(project, options);
    const jsonSchemas = generateJsonSchemas(options, project.getTypeChecker(), references);
    await outputSchemas(options, jsonSchemas);
    return jsonSchemas.length;
}

export function generateSchemasSync(options: GenerateSchemasOptions): number {
    const project = createProject(options);
    const references = findReferences(project, options);
    const jsonSchemas = generateJsonSchemas(options, project.getTypeChecker(), references);
    outputSchemasSync(options, jsonSchemas);
    return jsonSchemas.length;
}

export async function loadSchema(name: string): Promise<JsonSchema> {
    const callerPath = getCallerFilePath(1);
    const callerPackagePath = await getPackagePath(callerPath);
    const schemaOutputDir = getEnvOptions().schemaOutputDirectory
        || getPackageJsonOptions(callerPackagePath).schemaOutputDirectory
        || DEFAULT_SCHEMA_OUTPUT_DIR;
    const schemaDirectory = path.resolve(callerPackagePath, schemaOutputDir);
    const schemaFullPath = path.resolve(schemaDirectory, generateSchemaFilename(name));
    const schemaFileContent = await wrapErrorAsync(
        async () => fs.promises.readFile(schemaFullPath),
        () => new TypeGuardError(`Failed to load schema ${name} at path ${schemaFullPath}`),
    );
    return JSON.parse(schemaFileContent.toString());
}

export function loadSchemaSync(name: string): JsonSchema {
    const callerPath = getCallerFilePath(1);
    const callerPackagePath = getPackagePathSync(callerPath);
    const schemaOutputDir = getEnvOptions().schemaOutputDirectory
        || getPackageJsonOptions(callerPackagePath).schemaOutputDirectory
        || DEFAULT_SCHEMA_OUTPUT_DIR;
    const schemaDirectory = path.resolve(callerPackagePath, schemaOutputDir);
    const schemaFullPath = path.resolve(schemaDirectory, generateSchemaFilename(name));
    const schemaFileContent = wrapError(
        () => fs.readFileSync(schemaFullPath),
        () => new TypeGuardError(`Failed to load schema ${name} at path ${schemaFullPath}`),
    );
    return JSON.parse(schemaFileContent.toString());
}



function findReferences(project: Project, options: GenerateSchemasOptions): CallExpression[] {
    const sourceFile = project.getSourceFile(VALIDATOR_DTS_FILENAME)
        || project.getSourceFileOrThrow(VALIDATOR_TS_FILENAME);

    const callNodes = [
        createTypeValidator,
        createTypeValidatorSync,
        createTypeGuard,
        createTypeGuardSync,
    ].flatMap((func) => {
        const declaration = wrapError(
            () => sourceFile
                .getSymbolOrThrow()
                .getExportOrThrow(func.name)
                .getValueDeclarationOrThrow(),
            e => new TypeGuardError(`Could not find auto-type-guard symbol for ${func.name}: ${e.message}`),
        );

        return project
            .getLanguageService()
            .findReferences(declaration)
            .flatMap(r => r
                .getReferences()
                .filter((rr) => {
                    const filePath = rr.getSourceFile().getFilePath();
                    if (options.filter?.length
                        && !options.filter.find(filter => minimatch(filePath, filter))
                    ) {
                        return false;
                    }
                    return !options.ignore?.find(filter => minimatch(filePath, filter));

                })
                .map(rr => rr
                    .getNode()
                    .getParentIfKind(SyntaxKind.CallExpression))
                .filter((rr: CallExpression | undefined): rr is CallExpression => rr !== undefined));
    });

    if (options.debug) {
        console.log(`Found ${callNodes.length} references: ${callNodes.map(n => `\n - ${nodeToString(n)}`).join('')}`);
    }

    return callNodes;
}

type Schema = { name: string, schema: JsonSchema };
function generateJsonSchemas(options: GenerateSchemasOptions, typeChecker: TypeChecker, callNodes: CallExpression[]): Schema[] {
    const schemaGenerator = tsj.createGenerator({ tsconfig: options.tsconfigPath });
    const alreadyGenerated = new Map<string, Node[]>();
    const schemas: Schema[] = [];
    callNodes.forEach((callNode) => {
        if (callNode.getTypeArguments().length !== 1) {
            throw new TypeGuardError(`Expected 1 type argument [at ${nodeToString(callNode)}]`);
        }
        if (callNode.getArguments().length !== 1) {
            throw new TypeGuardError(`Expected 1 argument [at ${nodeToString(callNode)}]`);
        }
        const typeArg = wrapError(
            () => callNode
                // Non-null assertion safety: we checked `callNode.getTypeArguments().length` a few lines above
                .getTypeArguments()[0]!
                .asKindOrThrow(SyntaxKind.TypeReference),
            e => new TypeGuardError(`Expected type argument to be a type reference: ${e.message} [at ${nodeToString(callNode)}]`),
        );
        const typeArgStr = typeArg.getText();
        const argStr = callNode
            // Non-null assertion safety: we checked `callNode.getArguments().length` a few lines above
            .getArguments()[0]!
            .asKindOrThrow(SyntaxKind.StringLiteral)
            .getLiteralValue();
        if (typeArgStr !== argStr) {
            throw new TypeGuardError(
                `Expected argument value "${argStr}" to match type argument name "${typeArgStr}" [at ${nodeToString(callNode)}]`,
            );
        }

        const typeArgDeclarationNodes = wrapError(
            () => typeArg.getType().isInterface()
                ? typeArg
                    .getFirstChildIfKindOrThrow(SyntaxKind.Identifier)
                    .getSymbolOrThrow()
                    .getDeclarations()
                : typeArg
                    .getType()
                    .getAliasSymbolOrThrow()
                    .getDeclarations(),
            e => new TypeGuardError(
                `Failed to parse type argument; is it a valid type reference? Original error: ${e.message} [at ${nodeToString(callNode)}]`,
            ),
        );

        if (alreadyGenerated.has(typeArgStr)) {
            if (checkNodesEquality(alreadyGenerated.get(typeArgStr)!, typeArgDeclarationNodes)) {
                if (options.debug) console.log(`Schema for type ${typeArgStr} has already been generated`);
                return;
            } else {
                console.error(`Duplicate type found`);
                console.error(`- ${nodesToString(alreadyGenerated.get(typeArgStr)!)}`);
                console.error(`- ${nodesToString(typeArgDeclarationNodes)}`);
                const conflictingDeclarations = [alreadyGenerated.get(typeArgStr)!, typeArgDeclarationNodes].map(nodesToString);
                throw new TypeGuardError(`Found duplicate type name "${typeArgStr}". Declarations: [${conflictingDeclarations}]`);
            }
        }
        schemas.push({
            name: typeArgStr,
            schema: schemaGenerator.createSchemaFromNodes([typeArg.compilerNode]),
        });
        alreadyGenerated.set(typeArgStr, typeArgDeclarationNodes);
        if (options.debug) console.log(`Generated schema for type ${typeArgStr}`);
    });

    return schemas;
}

function outputSchemasSync(options: GenerateSchemasOptions, schemas: Schema[]): void {
    const outputDir = path.resolve(options.packagePath, options.schemaOutputDirectory);
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir, { recursive: true });
    }

    for (const { name, schema } of schemas) {
        const schemaPath = path.resolve(outputDir, generateSchemaFilename(name));
        fs.writeFileSync(schemaPath, JSON.stringify(schema, null, 2));
    }
}

async function outputSchemas(options: GenerateSchemasOptions, schemas: Schema[]): Promise<void> {
    const outputDir = path.resolve(options.packagePath, options.schemaOutputDirectory);
    try {
        await fs.promises.access(outputDir);
    } catch {
        await fs.promises.mkdir(outputDir, { recursive: true });
    }

    for (const { name, schema } of schemas) {
        const schemaPath = path.resolve(outputDir, generateSchemaFilename(name));
        await fs.promises.writeFile(schemaPath, JSON.stringify(schema, null, 2));
    }
}

function createProject(options: GenerateSchemasOptions): Project {
    const project = new Project({ tsConfigFilePath: options.tsconfigPath });
    const { name: packageName } = JSON.parse(fs.readFileSync(`${__dirname}/../../package.json`).toString());
    if (!project.getSourceFile(VALIDATOR_TS_FILENAME) && !project.getSourceFile(VALIDATOR_DTS_FILENAME)) {
        const dtsPath = path.resolve(options.packagePath, `node_modules/${packageName}/build/src/${VALIDATOR_DTS_FILENAME}`);
        project.addSourceFileAtPath(dtsPath);
        options.debug && console.log(`Manually adding ${VALIDATOR_DTS_FILENAME} to module resolution`);
    }
    if (options.debug) {
        console.log(`> Source files in project:`);
        project.getSourceFiles().forEach(sf => console.log(`- ${sf.getFilePath()}`));
    }
    return project;
}
function generateSchemaFilename(typeName: string): string {
    return `${typeName}.schema.json`;
}
function nodeToString(node: Node): string {
    return `${node.getSourceFile().getFilePath()}:${node.getStartLineNumber()}`;
}
function nodesToString(nodes: Node[]): string {
    return nodes.map(nodeToString).join('&');
}
function checkNodesEquality(nodes1: Node[], nodes2: Node[]): boolean {
    if (nodes1.length !== nodes2.length) {
        return false;
    }
    for (let i = 0; i < nodes1.length; i++) {
        if (nodes1[i] !== nodes2[i]) {
            return false;
        }
    }
    return true;
}
