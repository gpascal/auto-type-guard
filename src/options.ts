import * as fs from 'fs';
import * as path from 'path';
import * as process from 'process';


export type GenerateSchemasOptions = {
    packagePath: string;
    tsconfigPath: string;
    schemaOutputDirectory: string;
    ignore?: string[];
    filter?: string[];
    debug?: boolean;
};

export function getPackageJsonOptions(packagePath: string): Partial<GenerateSchemasOptions> {
    try {
        const packageJson = JSON.parse(fs.readFileSync(path.resolve(packagePath, 'package.json')).toString());
        const packageJsonConfig = packageJson['auto-type-guard'];
        const {
            tsconfigPath,
            schemaOutputDirectory,
            filter,
            ignore,
            debug,
        } = packageJsonConfig;
        return compactObject({
            tsconfigPath,
            schemaOutputDirectory,
            filter,
            ignore,
            debug,
        });
    } catch {
        return {};
    }
}

export function getEnvOptions(): Partial<GenerateSchemasOptions> {
    const {
        SCHEMA_OUTPUT_DIRECTORY,
        TSCONFIG_PATH,
        FILTER,
        IGNORE,
        DEBUG,
    } = process.env;
    return compactObject({
        tsconfigPath: TSCONFIG_PATH,
        schemaOutputDirectory: SCHEMA_OUTPUT_DIRECTORY,
        filter: FILTER ? FILTER.split(';') : undefined,
        ignore: IGNORE ? IGNORE.split(';') : undefined,
        debug: DEBUG === '1',
    });
}

function compactObject<T extends { [k: string]: unknown }>(input: T): Partial<T> {
    return Object.keys(input).reduce((compactedObject: Partial<T>, currentKey: keyof T) => {
        if (input[currentKey] != null && input[currentKey] !== '') {
            compactedObject[currentKey] = input[currentKey];
        }
        return compactedObject;
    }, {});
}
