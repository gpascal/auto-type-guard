import { ErrorObject } from 'ajv';

export class TypeGuardError extends Error {
    public readonly ajvErrors: ErrorObject[] | undefined;

    constructor(message: string, ajvErrors?: ErrorObject[]) {
        super(message);
        this.ajvErrors = ajvErrors;
    }
}

export function wrapError<T>(
    func: () => T,
    errorWrapper: (e: Error) => Error,
): T {
    try {
        return func();
    } catch(e) {
        throw errorWrapper(e);
    }
}

export async function wrapErrorAsync<T>(
    func: () => Promise<T> | T,
    errorWrapper: (e: Error) => TypeGuardError,
): Promise<T> {
    try {
        return await func();
    } catch(e) {
        throw errorWrapper(e);
    }
}
