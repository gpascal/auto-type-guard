# auto-type-guard

Automatically generate type guard and type assertion functions from Typescript types. 

Notable dependencies:
- The validation is based on json-schemas and uses [ts-json-schema-generator](https://github.com/vega/ts-json-schema-generator) to generate the schemas from Typescript types. JS-doc [annotations](https://github.com/YousefED/typescript-json-schema#annotations) are supported to add additional constraints to the generated schemas.
- The schema generation step uses [ts-morph](https://github.com/dsherret/ts-morph) to find calls to `createTypeValidator` and `createTypeGuard`
- Runtime validation uses [ajv](https://github.com/ajv-validator/ajv)

## Install and setup

- `npm install -S auto-type-guard`
- Call `generate-schemas .` somewhere in your build process (e.g. in the `postbuild` script of your `package.json`), or manually call the command when generated schemas need to be updated. Read [Configuration](#Configuration) for additional configuration options.
  - Alternatively, the exported `generateSchemas()` function can be called manually at runtime, but note that this requires your Typescript sources to be accessible.
- The json-schema output directory (which defaults to `<package-path>/json-schemas`) should be added to your `.gitignore` (but not in your `.npmignore`, since the schemas have to be packaged with the rest of your code).

## Usage

### Type assertion function

```ts
import { createTypeValidatorSync, TypeValidator } from 'auto-type-guard';

type Point = {
    x: number;
    y: number;
};

// The validator function needs to be declared with an explicit type annotation
// The string literal passed to createTypeValidator needs to be strictly equal to the type argument name
const pointValidator: TypeValidator<Point> = createTypeValidatorSync<Point>('Point');

const unknownData: unknown = { x: 12, y: 4 };
pointValidator(unknownData);

// unknownData type has been narrowed to Point
const { x, y } = unknownData;
```

### Type guard

```ts
import { createTypeGuardSync } from 'auto-type-guard';

type Point = {
    x: number;
    y: number;
};

// The string literal passed to createTypeGuard needs to be strictly equal to the type argument name
const pointGuard = createTypeGuardSync<Point>('Point');

const unknownData: unknown = { x: 12, y: 4 };
if (pointGuard(unknownData)) {
    // unknownData type has been narrowed to Point
    const { x, y } = unknownData;
}
```

## Configuration

[Read the configuration documentation here](./doc/configuration.md)

## Limitations

- The type argument passed to `createTypeValidator`/`createTypeGuard` needs to be a reference to an existing interface or type alias, it cannot be an inlined type (not even an array of type like `Point[]`). If you need to validate an array, define a type alias for this type:
  ```ts
  import { createTypeGuard } from './validator';
  
  type Point = { x: number; y: number };
  // This will not work
  const inlinedPointArrayGuard = createTypeGuard<Point[]>('PointArray');

  // So we define an alias for the array type and use it as the type argument
  type PointArray = Point[];
  const pointArrayGuard = createTypeGuard<PointArray>('PointArray');
  ```
- Overloaded interfaces are not handled properly, only the first definition of the interface will be accounted for.
- The string argument of `createTypeValidator`/`createTypeGuard` must be a string literal strictly equal to the name of the type argument. Because of this, you cannot generate schemas for 2 different types with the same name in the same codebase, unless if these are not generated at the same time (by using the `filter` or `ignore` options).
- Being json-schema based, this package can only validate json-compatible data, e.g. no functions, classes, buffers.
