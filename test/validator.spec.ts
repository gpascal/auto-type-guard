import * as fs from 'fs';
import * as path from 'path';

import { expect } from 'chai';

import { createTypeValidator, generateSchemas, TypeValidator } from '../src';
import { GenerateSchemasOptions } from '../src/options';


describe('> validator', () => {

    const generateOpts: GenerateSchemasOptions = {
        packagePath: `${__dirname}/../..`,
        schemaOutputDirectory: 'test/json-schemas',
        tsconfigPath: `${__dirname}/../../tsconfig.json`,
        filter: ['**/test/**/*.spec.ts'],
    };

    before(async function () {
        // Gitlab-CI pipelines can be pretty slow, hence the high timeout
        this.timeout(30000);
        process.env.SCHEMA_OUTPUT_DIRECTORY = generateOpts.schemaOutputDirectory;
        console.log(`Generating schemas...`);
        await generateSchemas(generateOpts);
    });

    after(async () => {
        await fs.promises.rm(
            path.resolve(generateOpts.packagePath, generateOpts.schemaOutputDirectory),
            { recursive: true },
        );
    });

    describe('> createTypeValidator', () => {

        type TestEntity = {
            strProp: string;
            numberProp: number;
            /** @asType integer */
            integerProp: number;
            /** @minimum 0
                @maximum 10 */
            boundedNumberProp: number;
        };

        let testEntityValidator: TypeValidator<TestEntity>;

        before(async () => {
            testEntityValidator = await createTypeValidator<TestEntity>('TestEntity');
        });

        it('> create json-schema files', async () => {
            const generatedFiles = await fs.promises.readdir(path.resolve(generateOpts.packagePath, generateOpts.schemaOutputDirectory));
            expect(generatedFiles).to.include('TestEntity.schema.json');
        });

        it('> correctly validate objects', () => {
            expect(() => testEntityValidator({
                strProp: 'string',
                numberProp: -12.4,
                integerProp: -5,
                boundedNumberProp: 4.5,
            })).to.not.throw();

            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: 0,
                boundedNumberProp: 0,
            })).to.not.throw();
        });

        it('> additional properties are forbidden by default', () => {
            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: 0,
                boundedNumberProp: 0,
                additionalProp: 0,
            })).to.throw();
        });

        it('> enforce basic types', () => {
            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: 0,
                boundedNumberProp: null,
            })).to.throw();

            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: '0',
                boundedNumberProp: 0,
            })).to.throw();
        });

        it('> enforce annotation-defined constraints', () => {
            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: 0.5,
                boundedNumberProp: 0,
            })).to.throw();

            expect(() => testEntityValidator({
                strProp: '',
                numberProp: 0,
                integerProp: 0,
                boundedNumberProp: 20,
            })).to.throw();
        });
    });

    describe('> unhandled types', () => {
        describe('> overloaded interface', () => {
            interface TestOverloadedInterface {
                propA: string;
            }
            interface TestOverloadedInterface {
                propB: string;
            }

            let testOverloadedInterfaceValidator: TypeValidator<TestOverloadedInterface>;

            before(async () => {
                testOverloadedInterfaceValidator = await createTypeValidator<TestOverloadedInterface>('TestOverloadedInterface');
            });

            it('> do not account for the interface overload', () => {
                expect(() => {
                    // This is valid according to TS
                    const value: TestOverloadedInterface = {
                        propA: 'a',
                        propB: 'b',
                    };
                    // But the validator refuses it (only the first definition of TestOverloadedInterface is accounted for, and
                    // additionalProperties is false, so propB is not accepted)
                    testOverloadedInterfaceValidator(value);
                }).to.throw();

                expect(() => {
                    testOverloadedInterfaceValidator({ propA: 'a' });
                }).to.not.throw();
            });
        });
    });
});
