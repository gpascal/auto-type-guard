import * as fs from 'fs';

import { expect } from 'chai';

import { getCallerFilePath, getPackagePathSync } from '../src/fs-utils';


describe('> fs-utils', () => {
    describe('> getCallerFilePath', () => {
        it('detect current file path', () => {
            const { name: packageName } = JSON.parse(fs.readFileSync(`${__dirname}/../../package.json`).toString());
            function caller(): string {
                return getCallerFilePath(0);
            }
            expect(caller()).to.include(`${packageName}/build/test/fs-utils.spec.js`);
        });
    });

    describe('> getPackagePath', () => {
        it('detect package.json path', () => {
            const packagePath = getPackagePathSync(__dirname);
            expect(packagePath).to.match(/\/auto-type-guard$/);
        });
    });
});
