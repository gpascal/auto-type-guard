module.exports = {
    root: true,
    parser: '@typescript-eslint/parser',
    parserOptions: {
        ecmaVersion: 2020,
        sourceType: 'module',
    },
    env: {
        es2020: true,
        mocha: true,
        node: true,
    },
    extends: [
        'plugin:import/errors',
        'plugin:import/warnings',
        'plugin:import/typescript',
        'eslint:recommended',
        'plugin:@typescript-eslint/eslint-recommended',
        'plugin:@typescript-eslint/recommended',
    ],
    plugins: [
        'import',
        '@typescript-eslint',
    ],
    rules: {
        '@typescript-eslint/no-non-null-assertion': 0,
        '@typescript-eslint/no-unused-vars': ['error', { varsIgnorePattern: '^_.*' }],
        'arrow-parens': ['error', 'as-needed', { requireForBlockBody: true }],
        'import/extensions': ['error', 'never'],
        'import/no-extraneous-dependencies': ['error', { devDependencies: ['**/test/**/*.spec.ts'] }],
        'import/no-unresolved': 0,
        'import/prefer-default-export': ['off'],
        indent: [
            'error',
            4,
            {
                SwitchCase: 0,
                FunctionDeclaration: {
                    parameters: 'first',
                    body: 1,
                },
                FunctionExpression: {
                    parameters: 'first',
                    body: 1,
                },
                CallExpression: { arguments: 1 },
                ArrayExpression: 1,
                ObjectExpression: 1,
                flatTernaryExpressions: false,
                ignoreComments: false,
                offsetTernaryExpressions: false,
            },
        ],
        'lines-between-class-members': ['error', 'always', { exceptAfterSingleLine: true }],
        'max-classes-per-file': 0,
        'max-len': ['error', 140],
        'no-unused-expressions': 'off',
        'no-unused-vars': 'off',
        'object-curly-newline': 0,
        quotes: ['error', 'single', { allowTemplateLiterals: true }],
        semi: ['error', 'always'],
    },
    ignorePatterns: ['build'],
    globals: { mocha: 'readonly' },
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                path: ['src'],
            },
        },
    },
};
